const tasks = [];

/**
 * 
 * List Task
 * 
 */
exports.list_task = (req, res, next) =>{
  res.status(200).json(tasks);
}

/**
 * 
 * Detail Task
 * 
 */
exports.detail_task = (req, res, next) =>{
  const id = req.params.id;
  const task = tasks.filter( taskDetail => taskDetail.id == id)
  res.status(200).json(task[0]);
}

/**
 * 
 * Add Task
 * 
 */
exports.add_task = (req, res, next) =>{
  const task = req.body;
  tasks.push(task);
  res.status(200).json(task);
}

/**
 * 
 * Edit Task
 * 
 */
exports.edit_task = (req, res, next) =>{
  const task = req.body;
  const id = req.params.id;
  const index = tasks.findIndex(taskDetail => taskDetail.id == id);
  tasks[index] = task;
  res.status(200).json(task);
}

/**
 * 
 * Status Task
 * Change the status of one task
 * 
 */
exports.status_task = (req, res, next) =>{
  const status = req.body.status;
  const id = req.params.id;
  const index = tasks.findIndex(taskDetail => taskDetail.id == id);
  tasks[index].status = status;
  res.status(200).json(tasks[index]);
}

/**
 * 
 * Delete Task
 * 
 */
exports.delete_task = (req, res, next) =>{
  const id = req.params.id;
  const index = tasks.findIndex(taskDetail => taskDetail.id == id);
  tasks.slice(index, 1);
  //tasks = tasks.filter(task => task.id != id);
  res.status(200).json({message:'Task deleted'});
}